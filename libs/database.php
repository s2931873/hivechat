<?php
function create_table(){

$dbhandle = sqlite_open('db/test.db', 0666, $error);
if (!$dbhandle) die ($error);

$stm = "CREATE TABLE Friends(Id integer PRIMARY KEY," . 
       "Name text UNIQUE NOT NULL, Sex text CHECK(Sex IN ('M', 'F')), Hash text UNIQUE NOT NULL)";
$ok = sqlite_exec($dbhandle, $stm, $error);

sqlite_close($dbhandle);

}
?>