<?php
//should already be defined in config.php
//include('Crypt/RSA.php');

//key files are created in config
//using standard io

class encryption
{

private $message;
private $privatekey;
private $publickey;

public function __construct($p_path, $pr_path, $string)
{
	$publickey = get_key($p_path);
	$privatekey = get_key($pr_path);
	$message = $string;
}

private function get_key($keyfile)
{
	$key = new Crypt_RSA();
    $key->loadKey(file_get_contents($keyfile));
	return $key;
}

//Function for encrypting with RSA
private function rsa_encrypt($public_key)
{
    //Create an instance of the RSA cypher and load the key into it
    $cipher = new Crypt_RSA();
    $cipher->loadKey($public_key);
    //Set the encryption mode
	$string = base64_encode($message);
	$message = $cipher->encrypt($string);
    //Return the encrypted version
    return $message;
}

//Function for decrypting with RSA 
private function rsa_decrypt($private_key)
{
    //Create an instance of the RSA cypher and load the key into it
    $cipher = new Crypt_RSA();
    $cipher->loadKey($private_key);
    //Set the encryption mode
    $string = base64_decode($message);
    //Return the decrypted version
	$message = $cipher->decrypt($string);
    return $message;
}

protected function message_output()
{
	return $message;
}

}
?>